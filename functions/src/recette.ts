import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import { isNotEmptyJson } from './shared';

const limit = 7;

export const getRecetteService = (request: functions.https.Request, response: functions.Response) => {
	admin
		.firestore()
		.collection("recette")
		.doc(request.query.id)
		.get()
		.then(snapshot => {
			response.status(200).send({recette: snapshot.data()});
		})
		.catch(reason => {
			response.status(500).send(reason);
		});
}

export const getRecettesService = (request: functions.https.Request, response: functions.Response) => {
    let recettes: any[] = [];
		admin
		.firestore()
		.collection("recette")
		.orderBy("dateCreation", "desc")
		.offset((limit * request.query.offset) - request.query.offset)
		.limit(limit)
		.get()
		.then(snapshot => {
			snapshot.forEach(doc => {
				if (isNotEmptyJson(doc.data())) {
					recettes = recettes.concat(doc.data());
				}
			});
			response.status(200).send({recettes: recettes, next: snapshot.size===limit});
		})
		.catch(reason => {
			response.status(500).send(reason);
		});
}

export const postRecetteService = (request: functions.https.Request, response: functions.Response) => {
    if (isNotEmptyJson(request.body)) {
        admin
            .firestore()
            .collection("recette")
            .doc()
            .get()
            .then(snap => {
                const recette = {
                    ...request.body,
                    dateCreation: admin.firestore.FieldValue.serverTimestamp(),
                    id: snap.ref.id
                };
                admin
                    .firestore()
                    .collection("recette")
                    .doc(snap.ref.id)
                    .set(recette)
                    .then(() => response.status(200).send(recette))
                    .catch(err => response.status(500).send(err));
            })
            .catch(err => response.status(500).send(err));
    }
}

export const putRecetteService = (request: functions.https.Request, response: functions.Response) => {
    if (isNotEmptyJson(request.body)) {

        const recette = request.body;
        recette.dateCreation = new admin.firestore.Timestamp(recette.dateCreation._seconds, recette.dateCreation._nanoseconds);

        admin
        .firestore()
        .collection("recette")
        .doc(recette.id)
        .set(recette)
        .then(() => response.status(200).send(recette))
        .catch(err => response.status(500).send(err));
    }
} 