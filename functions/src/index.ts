import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import * as cors from "cors";
import { uploadImageRecetteService, uploadImageGalerieService, getAllImageGalerieService } from './image';
import { getRecetteService, getRecettesService, postRecetteService, putRecetteService } from './recette';

const corsHandler = cors({ origin: true });


//READ ALL RECETTE
export const getRecettes = functions.https.onRequest((request, response) => {	
	// tslint:disable-next-line: no-empty
	corsHandler(request, response, () => {
		getRecettesService(request, response);
	});
	
});

//READ RECETTE
export const getRecette = functions.https.onRequest((request, response) => {
	// tslint:disable-next-line: no-empty
	corsHandler(request, response, () => {
		getRecetteService(request, response);
	});

});

//POST RECETTE
export const addRecette = functions.https.onRequest((request, response) => {
	// tslint:disable-next-line: no-empty
	corsHandler(request, response, () => {
		let idToken: string | undefined;
		idToken = request.get("Authorization");
		admin.auth().verifyIdToken(!!idToken ? idToken : '')
			.then(() => 
				postRecetteService(request, response)
			)
			.catch(err => response.status(500).send(err));
	});
});

//PUT RECETTE
export const updateRecette = functions.https.onRequest((request, response) => {
	// tslint:disable-next-line: no-empty
	corsHandler(request, response, () => {
		let idToken: string | undefined;
		idToken = request.get("Authorization");
		admin.auth().verifyIdToken(!!idToken ? idToken : '')
			.then(() => 
				putRecetteService(request, response)
			)
			.catch(err => response.status(500).send(err));
	});
});


//GET ALL IMAGES
export const getAllImageGalerie = functions.https.onRequest((request, response) => {
	// tslint:disable-next-line: no-empty
	corsHandler(request, response, () => {
		getAllImageGalerieService(request, response);
	});
});

//UPLOAD IMAGE RECETTE
export const uploadImageRecette = functions.https.onRequest((request, response) => {
	corsHandler(request, response, () => {
		let idToken: string | undefined;
		idToken = request.get("Authorization");
		admin.auth().verifyIdToken(!!idToken ? idToken : '')
			.then(() => {
				uploadImageRecetteService(request, response);		
			}).catch(err => {return response.status(500).send(err)});
	});
});

//UPLOAD IMAGE GALERIE
export const uploadImageGalerie = functions.https.onRequest((request, response) => {
	corsHandler(request, response, () => {
		let idToken: string | undefined;
		idToken = request.get("Authorization");
		admin.auth().verifyIdToken(!!idToken ? idToken : '')
			.then(() => {
				uploadImageGalerieService(request, response);
			}).catch(err => {return response.status(500).send(err)});
	});
});

/* export const deleteRecette = functions.https.onRequest((request, response) => {
	// tslint:disable-next-line: no-empty
	corsHandler(request, response, () => {
		admin
		.firestore()
		.collection("recette")
		.doc(request.query.id)
		.delete()
		.then(() => {return response.status(200).send()})
		.catch(err => {return response.status(500).send(err)});
	});	
});

export const migrateImage = functions.https.onRequest((request, response) => {
	// tslint:disable-next-line: no-empty
	corsHandler(request, response, () => {
		admin
		.firestore()
		.collection("image")
		.get()
		.then(snapshot => {
			snapshot.forEach(doc => {

			admin
			.firestore()
			.collection("imageRecette")
			.doc(doc.ref.id)
			.get()
			.then(() => {
				admin
				.firestore()
				.collection("imageRecette")
				.doc(doc.ref.id)
				.set(doc.data())
				.then((data) => 
					{ console.log(data)}
				).catch(reason => {
					response.status(500).send(reason);
				});			

			}).catch(reason => {
				response.status(500).send(reason);
			});
			response.status(200).send();
			});		
		}).catch(reason => {
			response.status(500).send(reason);
		});	
	});
}); */